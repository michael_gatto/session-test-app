<!--- We should see a struct of cache.connections --->
<cfdump var="#getApplicationSettings()#" />

<!--- Examine session raw from Redis --->
<cfdump var="#session#" />

<!--- Attempt to call methods on CFC stored in the session --->
<cfoutput>
    #session.mycfc.getName()#
    #session.mycfc.sayHello()#
</cfoutput>

<!--- Seed the session from Lucee --->
<cfscript>
    import Person;

    session.mystring = "this is my string in the session";

    session.mynumeric = 77;

    session.mylist = "one,two,three";

    session.myarray = [ 1, 0, "I'm a string with a quote in it."];

    session.mystruct = { pretend_key = "really crucial data here" };

    session.mycfc = new Person(1);
    session.mycfc.setName("Jim Kirk");

    session.myquery = queryNew("pk,fk,data", "integer,integer,varchar", [
        [1, 10, "aa"],
        [2, 20, "bb"],
        [3, 20, "cc"],
        [4, 30, "dd"],
        [5, 30, "ee"],
        [6, 30, "ff"]
    ]);

    session.hitcount = (session.hitcount ?: 0) + 1;

    // smoke test to ensure the redis cache is available
    cachePut("cached_string_1011", "A string for redis!", createTimeSpan(0, 0, 120, 0), createTimeSpan(0, 0, 120, 0), "redis");
</cfscript>
<cfoutput><br /> Cache Redis: #cacheGet("cached_string_1011", "redis")#</cfoutput>

<!--- Get a string saved only in Spring --->
<cfdump var="#cacheGet("cached_string_777", "redis")#" label="From Spring!" />
<cfoutput>
    #session.mycfc.getName()#
    #session.mycfc.sayHello()#
</cfoutput>

<!--- Update object properties in the session-stored CFC --->
<cfset session.mycfc.setName("Spock") />
<cfoutput>#session.mycfc.getName()#</cfoutput>
<cfdump var="#session#" /><br />
