component {
    this.name = "cluster-app-02";
    this.applicationTimeout = createTimeSpan(0, 0, 30, 0);

    // set datasources here, too. perfect for dockeri-izing:
    // https://bitbucket.org/lucee/lucee/wiki/Cookbook_Datasource_Define_Datasource

    /* "servers": server.system.environment["SESSION_MEMCACHED_SERVERS"] ?: "" */
    //this.cache.connections
    //
    if (structKeyExists(server.system.environment, "LUCEE_APPLICATION_SESSIONSTORAGE")) {
        switch (server.system.environment["LUCEE_APPLICATION_SESSIONSTORAGE"]) {
            case "redis":
                /*
                    server.system.environment["SESSION_MEMCACHED_SERVERS"] ?: ""
                    LUCEE_APPLICATION_SESSIONCLUSTER
                    LUCEE_APPLICATION_SESSIONSTORAGE
                 */
                 this.cache["redis"] = {
                 	  class: 'lucee.extension.io.cache.redis.RedisSentinelCache'
                 	, bundleName: 'redissentinel.extension'
                 	, bundleVersion: '0.0.1.29'
                 	, storage: true
                 	, custom: {
                 		"masterName":"vinsuite",
                 		"sentinels":"10.14.15.102:26379
                                     10.14.15.103:26379
                                     10.14.15.104:26379",
                 		"namespace":"lucee:cache"
                 	}
                 	, default: 'object'
                 };

                break;

            default:
        }
    }



    this.cache.object = "redis";
    this.cache.function = "redis";

    // session stuff
    this.clientManagement = false;
    this.sessionManagement = true;
    this.sessionStorage = "redis";
    this.sessionTimeout = createTimeSpan(0, 2, 0, 0);
    this.sessionCluster = true;
    this.sessionType="application";  // jee is not suitable for clustering


    boolean function onApplicationStart() {
        return true;
    }

    void function onApplicationEnd(struct application) {
    }


    void function onSessionStart() {
        echo("Session started.");
    }

    void function onSessionEnd(struct application,struct session) {
        echo("Session ended.");
    }

    boolean function onRequestStart(string targetPage) {
        echo('<html><head/><body><h1>Session Test</h1>');

        return true;
    }

    void function onRequestEnd(string targetPage) {
        echo('</body></html>');
    }
}
