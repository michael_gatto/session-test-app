/**
 * A test cfc for sessions
 **/
component accessors="true" output="false" persistent="false" {
    property numeric id;
    property string name;

    var canYouSeeMee = "Am I public or private, I don't know!";

    function init(required numeric person_id) {
        this.setId(arguments.person_id);

        return this;
    }

    /**
     * Test method to ensure explicitly defined methods can operate when its
     * object is saved in a cache.
     */
    public any function sayHello() {
        return "Hello, I am " & this.getName();
    }
}
